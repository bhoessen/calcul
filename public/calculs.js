/**
 * Class representing a question that can be asked in an exercice
 */
class Question {
    //The string representation of the question
    #m_question
    //The array containing the different response proposals, only one is correct
    #m_proposals
    //The index of the correct proposal
    #m_correct_index
    //Data that is being passed along the question
    #m_data
    //The callback used whenever the answer is wrongly answered
    #m_bad_answer
    //The callback used whenever the answer is correclty answered
    #m_good_answer

    /**
     * Create a new question
     * @param {string} question The string representation of the question to ask
     * @param {Array} proposals The different answers proposed to the user
     * @param {Number} correct_index The index of the correct answer in the proposal array
     * @param {Object} data A generic object that can be used to provide data along the question
     * @param {Function(Question)} callback_good_answer The callback that will be called whenever a good answer is given. Can be null
     * @param {Function(Question, Number)} callback_bad_answer The callback that will be called whenever a bad answer is given.
     * The number parameter is the index of the wrong answer given. Can be null
     */
    constructor(question, proposals, correct_index, data, callback_good_answer, callback_bad_answer) {
        this.#m_question = question;
        this.#m_proposals = proposals;
        this.#m_correct_index = correct_index;
        this.#m_good_answer = callback_good_answer;
        this.#m_bad_answer = callback_bad_answer;
        this.#m_data = data;
    }

    /**
     * The data passed along the question
     * @returns Object
     */
    get_data() {
        return this.#m_data;
    }

    /**
     * The callback that will be called whenever a good answer is given. Can be null
     * @returns Function(Question)
     */
    get_good_answer_callback() {
        return this.#m_good_answer;
    }

    /**
     * The callback that will be called whenever a bad answer is given.
     * The number parameter is the index of the wrong answer given. Can be null.
     * @returns Function(Question, Number)
     */
    get_bad_answer_callback() {
        return this.#m_bad_answer;
    }

    /**
     * Retrieve the string representation of the question to ask to the user
     * @returns string
     */
    get_question() {
        return this.#m_question;
    }

    /**
     * Retrieve the different propositions for answers
     * @returns Array
     */
    get_proposals() {
        return this.#m_proposals;
    }

    /**
     * Retrieve the index of the correct answer proposal in the array of proposals
     * @returns Number
     */
    get_correct_index() {
        return this.#m_correct_index;
    }

}

/**
 * This class represent the logic to ask questions and check their validities to the user
 */
class Exercice {
    //The number correct answers given by the user until now
    #m_points
    //The number of questions that were submitted to the user
    #m_tries
    //The total time taken to answer the questions
    #m_time_taken
    //The current question
    #m_question
    //The Date object containing the moment the question was asked
    #m_question_start
    //The lambda used to generate the questions objects
    #m_question_gen
    //The div used to display the number of questions submitted
    #m_tries_div
    //The div used to display the average time to answer a question
    #m_avg_time_div
    //The div used to display the question
    #question_div
    //The div used to display the number of correct answers given
    #m_success_div
    //The array containing the <button> that the user can select
    #m_buttons


    /**
     * Create a new instance of exercice. The different elements should be provided to specify the UI
     * @param {HTMLDivElement} question_div The div that will contain the question
     * @param {HTMLButtonElement[]} buttons The list of buttons that proposes answers
     * @param {HTMLElement} success The element that will see its innerHTML set to the number of points
     * @param {HTMLElement} tries The element that will see its innerHTML set to the number of tries
     * @param {HTMLElement} avg_time The element that will see its innerHTML set to the average time taken per question
     * @param {Function(): string} question_generator The lambda that is used to create a function
     * and whether the proposal is correct or not (the boolean)
     */
    constructor(question_div, buttons, success, tries, avg_time, question_generator) {
        console.assert(question_div != null);
        console.assert(buttons != null);
        console.assert(question_generator != null);
        this.#m_buttons = buttons;
        this.#m_success_div = success;
        this.#m_tries_div = tries;
        this.#m_avg_time_div = avg_time;
        this.#question_div = question_div;
        this.#m_points = 0;
        this.#m_tries = 0;
        this.#m_time_taken = 0;
        this.#m_question_gen = question_generator;
        this.#m_question = null;
        this.#m_question_start = new Date();
    }

    /**
     * Start a new game
     */
    start_questionnaire() {
        this.#clear_colors();
        this.#m_question = this.#m_question_gen();
        this.#question_div.innerHTML = this.#m_question.get_question();
        let proposals = this.#m_question.get_proposals();
        console.assert(this.#m_buttons.length === proposals.length);
        let c_this = this;
        for (let index = 0; index < this.#m_buttons.length; index++) {
            this.#m_buttons[index].innerHTML = proposals[index];
            if (index === this.#m_question.get_correct_index()) {
                this.#m_buttons[index].onclick = function () { c_this.#good_answer(); }
            } else {
                this.#m_buttons[index].onclick = function () { c_this.#bad_answer(index); }
            }
        }
        this.#m_question_start = new Date();
    }

    /**
     * Update the UI with the new counts (for tries, successes, average time, ...)
     */
    #update_ui() {
        this.#m_tries += 1;

        let time_diff = new Date() - this.#m_question_start;
        this.#m_time_taken += time_diff;
        let m_avg_time_taken = this.#m_time_taken / this.#m_tries;

        if (this.#m_success_div) {
            this.#m_success_div.innerHTML = this.#m_points;
        }
        if (this.#m_tries_div) {
            this.#m_tries_div.innerHTML = this.#m_tries;
        }
        if (this.#m_avg_time_div) {
            this.#m_avg_time_div.innerHTML = (m_avg_time_taken / 1000.0).toFixed(2);
        }
        this.#color_answers();
    }

    /**
     * Function called whenever the user click on a wrong answer
     */
    #bad_answer(index) {
        this.#update_ui();
        let callback = this.#m_question.get_bad_answer_callback();
        if (callback) {
            callback(this.#m_question, index);
        }
        let c_this = this;
        setTimeout(function () {
            c_this.start_questionnaire()
        }, 2500);
    }

    /**
     * Function called whenever the user click on the good answer
     */
    #good_answer() {
        this.#m_points += 1;
        this.#update_ui();
        let callback = this.#m_question.get_good_answer_callback();
        if (callback) {
            callback(this.#m_question);
        }
        let c_this = this;
        setTimeout(function () {
            c_this.start_questionnaire()
        }, 2500);
    }

    /**
     * Change the background color of the buttons to show which proposals
     * were correct or bad
     */
    #color_answers() {
        for (let i = 0; i < this.#m_buttons.length; i++) {
            let btn = this.#m_buttons[i];
            if (i != this.#m_question.get_correct_index()) {
                btn.style.backgroundColor = "rgb(233,73,77)";
            } else {
                btn.style.backgroundColor = "rgb(73,233,77)";
            }
            btn.onclick = null;
        }
    }

    /**
     * Remove all the background colors of the buttons
     */
    #clear_colors() {
        for (const btn of this.#m_buttons) {
            btn.style.backgroundColor = "";
        }
    }

}